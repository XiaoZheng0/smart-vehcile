package com.tedu.ivos.vehicle.service.impl;

import com.tedu.ivos.vehicle.mapper.VehicleMapper;
import com.tedu.ivos.vehicle.pojo.dto.VehicleQuery;
import com.tedu.ivos.vehicle.pojo.dto.VehicleSaveParam;
import com.tedu.ivos.vehicle.pojo.entity.Vehicle;
import com.tedu.ivos.vehicle.pojo.vo.VehicleVO;
import com.tedu.ivos.vehicle.service.VehicleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    VehicleMapper vehicleMapper;
    @Override
    public List<VehicleVO> selectVehicle(VehicleQuery vehicleQuery) {
        log.debug("查询车辆业务,参数:{}",vehicleQuery);
        List<VehicleVO> list=vehicleMapper.selectVehicle(vehicleQuery);
        return list;

    }

    @Override
    public void saveVehicle(VehicleSaveParam vehicleSaveParam) {
        log.debug("保存车辆业务,参数:{}",vehicleSaveParam);
        Vehicle vehicle = new Vehicle();
        BeanUtils.copyProperties(vehicleSaveParam,vehicle);
        if(vehicle.getId() == null){
            vehicle.setCreateTime(new Date());
            vehicle.setStatus("1");//设置车辆状态为“空闲”
            vehicle.setGeofenceBindStatus("0");//车辆初始的围栏绑定状态为0，未绑定任何围栏
            vehicleMapper.insert(vehicle);
        }else{
            vehicle.setUpdateTime(new Date());
            vehicleMapper.update(vehicle);
        }
    }

    @Override
    public void deleteVehicle(Long id) {
        log.debug("删除车辆业务,参数:{}",id);
        vehicleMapper.deleteVehicle(id);
    }

    @Override
    public void unbindVehicle(Long vehicleId) {
        log.debug("解绑车辆业务,参数:{}",vehicleId);
        Vehicle vehicle = new Vehicle();
        vehicle.setId(vehicleId);
        vehicle.setGeofenceBindStatus("0");
        vehicle.setGeofenceId(null);
        vehicle.setUpdateTime(new Date());
        vehicleMapper.updateNullValue(vehicle);
    }

    @Override
    public void bindVehicle(Long geofenceId, long vehicleId) {
        log.debug("绑定车辆业务,参数:={},{}",geofenceId,vehicleId);
        Vehicle vehicle = new Vehicle();
        vehicle.setId(vehicleId);
        vehicle.setGeofenceBindStatus("1");
        vehicle.setGeofenceId(geofenceId);
        vehicle.setUpdateTime(new Date());
        vehicleMapper.update(vehicle);
    }
}
