package com.tedu.ivos.vehicle.service;

import com.tedu.ivos.vehicle.pojo.dto.VehicleQuery;
import com.tedu.ivos.vehicle.pojo.dto.VehicleSaveParam;
import com.tedu.ivos.vehicle.pojo.vo.VehicleVO;

import java.util.List;

public interface VehicleService {
    List<VehicleVO> selectVehicle(VehicleQuery vehicleQuery);

    void saveVehicle(VehicleSaveParam vehicleSaveParam);

    void deleteVehicle(Long id);

    void unbindVehicle(Long vehicleId);

    void bindVehicle(Long geofenceId, long vehicleId);
}
