package com.tedu.ivos.base.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.tedu.ivos.*.mapper")
public class MybatisConfig {

}