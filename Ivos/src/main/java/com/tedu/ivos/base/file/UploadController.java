package com.tedu.ivos.base.file;

import com.tedu.ivos.base.response.JsonResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.spring.web.json.Json;

@RestController
@RequestMapping("/v1/file")
public class UploadController {

    @PostMapping("upload")
    //MultipartFile用来接收上传图片的对象
    //注意：形参名file必须与前端的upload图片上传组件的name值一致
    public JsonResult upload(MultipartFile file){
        return JsonResult.ok();

    }

}
