package com.tedu.ivos.user.mapper;

import com.tedu.ivos.user.pojo.dto.UserQuery;
import com.tedu.ivos.user.pojo.entity.User;
import com.tedu.ivos.user.pojo.vo.UserVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {

    UserVO selectByUsername(String username);

    void insert(User user);

    List<UserVO> selectUser(UserQuery userQuery);

    void update(User user);

    void deleteById(Long userId);

    UserVO selectById(Long parentId);
}
