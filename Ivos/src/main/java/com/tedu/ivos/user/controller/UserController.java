package com.tedu.ivos.user.controller;

import com.tedu.ivos.base.response.JsonResult;
import com.tedu.ivos.user.pojo.dto.UserLoginParam;
import com.tedu.ivos.user.pojo.dto.UserQuery;
import com.tedu.ivos.user.pojo.dto.UserSaveParam;
import com.tedu.ivos.user.pojo.vo.UserVO;
import com.tedu.ivos.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/v1/user")
public class UserController {

    @Autowired
    UserService userService;
    @PostMapping("login")
    public JsonResult login(@RequestBody UserLoginParam userLoginParam){
        log.debug("用户登录:参数={}",userLoginParam);
        UserVO userVO = userService.login(userLoginParam);
        return JsonResult.ok(userVO);
    }

    @PostMapping("save")
    public JsonResult saveUser(UserSaveParam userSaveParam){
        log.debug("保存用户:参数={}",userSaveParam);
        userService.saveUser(userSaveParam);
        return JsonResult.ok();
    }

    @GetMapping("select")
    public JsonResult selectUser(UserQuery userQuery){
        log.debug("查询用户:参数={}",userQuery);
        List<UserVO> list=userService.selectUser(userQuery);
        return JsonResult.ok(list);
    }

    @PostMapping("/update/password/{userId}")
    public JsonResult resetPassword(@PathVariable Long userId){
        log.debug("重置密码,参数={}",userId);
        userService.resetPassword(userId);
        return JsonResult.ok();
    }

    @PostMapping("/update/status/{userId}/{status}")
    public JsonResult updateStatus(@PathVariable Long userId,@PathVariable String status){
        log.debug("修改用户状态,参数:{},{}",userId,status);
        userService.updateStatus(userId,status);
        return JsonResult.ok();
    }

    @PostMapping("/delete/{userId}")
    public JsonResult deleteUser(@PathVariable Long userId){
        log.debug("删除员工,参数={}",userId);
        userService.delete(userId);
        return JsonResult.ok();
    }

    @GetMapping("/select/audit/{parentId}")
    public JsonResult selectAuditList(@PathVariable Long parentId){
        log.debug("查询审批人列表：参数={}",parentId);
        List<UserVO> list = userService.selectAuditList(parentId);
        return JsonResult.ok(list);
    }
}
