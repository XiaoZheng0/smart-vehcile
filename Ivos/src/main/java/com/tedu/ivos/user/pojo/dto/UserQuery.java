package com.tedu.ivos.user.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserQuery {
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "状态")
    private String status;
    @ApiModelProperty(value = "用户id")
    private Long id;//编辑时需要回显待编辑用户时使用
    @ApiModelProperty(value = "职级")
    private String level;//新增时加载带选直属领导选项时使用
    @ApiModelProperty(value = "直属领导id")
    private Long parentId;//查询直属领导数据时使用
}
