package com.tedu.ivos.user.service;

import com.tedu.ivos.user.pojo.dto.UserLoginParam;
import com.tedu.ivos.user.pojo.dto.UserQuery;
import com.tedu.ivos.user.pojo.dto.UserSaveParam;
import com.tedu.ivos.user.pojo.vo.UserVO;

import java.util.List;

public interface UserService {
    UserVO login(UserLoginParam userLoginParam);

    void saveUser(UserSaveParam userSaveParam);

    List<UserVO> selectUser(UserQuery userQuery);

    void resetPassword(Long userId);

    void updateStatus(Long userId, String status);

    void delete(Long userId);

    List<UserVO> selectAuditList(Long parentId);
}
