package com.tedu.ivos.user.service.impl;

import com.tedu.ivos.base.exception.ServiceException;
import com.tedu.ivos.base.response.StatusCode;
import com.tedu.ivos.user.mapper.UserMapper;
import com.tedu.ivos.user.pojo.dto.UserLoginParam;
import com.tedu.ivos.user.pojo.dto.UserQuery;
import com.tedu.ivos.user.pojo.dto.UserSaveParam;
import com.tedu.ivos.user.pojo.entity.User;
import com.tedu.ivos.user.pojo.vo.UserVO;
import com.tedu.ivos.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserVO login(UserLoginParam userLoginParam) {
        log.debug("登录业务,参数:{}",userLoginParam);
        //根据前端传来的用户查询数据库中是否有此用户
        UserVO userVO=userMapper.selectByUsername(userLoginParam.getUsername());
        //如果业务层遇到了问题，不可以直接给前端响应，要抛出异常，由全局异常处理器来处理
        if(userVO==null){
            throw new ServiceException(StatusCode.USERNAME_ERROR);
        }

        if(!userVO.getPassword().equals(userLoginParam.getPassword())){
            throw new ServiceException(StatusCode.PASSWORD_ERROR);
        }
        //如果没有异常，业务层会将正常处理完毕的userVO结果返回给Controller层
        //再由Controller层响应给前端
        return userVO;
    }

    @Override
    public void saveUser(UserSaveParam userSaveParam) {
        log.debug("保存用户业务,参数:{}",userSaveParam);
        User user = new User();
        BeanUtils.copyProperties(userSaveParam,user);
        //需要判断是新增还是修改操作
        if(user.getId()==null){
            user.setPassword("123456");
            user.setCreateTime(new Date());
            userMapper.insert(user);

        }else {
            //TODO 更新用户
            user.setUpdateTime(new Date());
            userMapper.update(user);
        }
    }

    @Override
    public List<UserVO> selectUser(UserQuery userQuery) {
        log.debug("查询用户业务,参数={}",userQuery);
        List<UserVO> list=userMapper.selectUser(userQuery);
        return list;
    }

    @Override
    public void resetPassword(Long userId) {
        log.debug("重置密码业务,参数={}",userId);
        User user = new User();
        user.setId(userId);
        user.setPassword("root");
        user.setUpdateTime(new Date());
        userMapper.update(user);
    }

    @Override
    public void updateStatus(Long userId, String status) {
        log.debug("修改用户状态业务,参数:{},{}",userId,status);
        User user = new User();
        user.setId(userId);
        user.setStatus(status);
        user.setUpdateTime(new Date());
        userMapper.update(user);
    }

    @Override
    public void delete(Long userId) {
        log.debug("删除用户业务,参数={}",userId);
        userMapper.deleteById(userId);
    }

    @Override
    public List<UserVO> selectAuditList(Long parentId) {
        log.debug("查询审批人业务:parentId={}",parentId);
        //1.准备一个空集合,用来保存查出来的多个审批人
        ArrayList<UserVO> userVOList = new ArrayList<>();
        //2.先根据传过来的上级id将直属领导查出来,并装到上方的空集合里
        UserVO auditUser1 = userMapper.selectById(parentId);
        userVOList.add(auditUser1);
        //3.如果有直属领导,且直属领导还有上级
        if(auditUser1 != null && auditUser1.getParentId() != null){
            //4.根据直属领导的上级id将上上级领导查出来,并装到上方的空集合里
            UserVO auditUser2 = userMapper.selectById(auditUser1.getParentId());
            userVOList.add(auditUser2);
        }
        //5.将准备好的审批人集合返回给controller
        return userVOList;

    }


}
