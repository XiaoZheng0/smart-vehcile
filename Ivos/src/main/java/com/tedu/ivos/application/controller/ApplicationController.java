package com.tedu.ivos.application.controller;

import com.tedu.ivos.application.mapper.ApplicationMapper;
import com.tedu.ivos.application.service.ApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping()
public class ApplicationController {

    @Autowired
    ApplicationService applicationService;
}
