package com.tedu.ivos.geofence.mapper;

import com.tedu.ivos.geofence.pojo.dto.GeofenceQuery;
import com.tedu.ivos.geofence.pojo.entity.Geofence;
import com.tedu.ivos.geofence.pojo.vo.GeofenceVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GeofenceMapper {
    List<GeofenceVO> selectGeofence(GeofenceQuery geofenceQuery);

    void update(Geofence geofence);

    void insert(Geofence geofence);

    void deleteGeofence(Long id);
}
