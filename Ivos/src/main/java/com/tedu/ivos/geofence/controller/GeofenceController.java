package com.tedu.ivos.geofence.controller;

import com.tedu.ivos.base.response.JsonResult;
import com.tedu.ivos.geofence.pojo.dto.GeofenceParam;
import com.tedu.ivos.geofence.pojo.dto.GeofenceQuery;
import com.tedu.ivos.geofence.pojo.vo.GeofenceVO;
import com.tedu.ivos.geofence.service.GeofenceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/geofence")
public class GeofenceController {
    @Autowired
    GeofenceService geofenceService;

    @GetMapping("select")
    public JsonResult selectGeofence(GeofenceQuery geofenceQuery){
        log.debug("查询电子围栏信息,参数:{}",geofenceQuery);
        List<GeofenceVO> list = geofenceService.selectGeofence(geofenceQuery);
        return JsonResult.ok(list);
    }

    @PostMapping("save")
    public JsonResult saveGeofence(GeofenceParam geofenceParam){
        log.debug("保存电子围栏信息,参数：{}",geofenceParam);
        geofenceService.saveGeofence(geofenceParam);
        return JsonResult.ok();
    }

    @PostMapping("/delete/{id}")
    public JsonResult deleteGeofence(@PathVariable Long id){
        log.debug("删除电子围栏信息,参数:{}",id);
        geofenceService.deleteGeofence(id);
        return JsonResult.ok();
    }
}
