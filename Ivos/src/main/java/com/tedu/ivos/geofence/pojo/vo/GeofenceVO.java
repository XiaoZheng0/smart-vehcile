package com.tedu.ivos.geofence.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tedu.ivos.vehicle.pojo.vo.VehicleVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class GeofenceVO {
    @ApiModelProperty(value = "电子围栏编号")
    private Long id;
    @ApiModelProperty(value = "电子围栏名称")
    private String name;
    @ApiModelProperty(value = "电子围栏状态")
    private String status;
    @ApiModelProperty(value = "电子围栏位置数据")
    private String position;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /* 下面的这三个字段会在分配车辆时使用,需要查看围栏上绑定的车辆总数、车辆列表 */
    @ApiModelProperty(value = "电子围栏绑定车辆总数")
    private Integer totalNum;
    @ApiModelProperty(value = "电子围栏可用车辆总数")
    private Integer availableNum;
    @ApiModelProperty(value = "电子围栏绑定车辆列表")
    private List<VehicleVO> vehiclVOList;
}
