package com.tedu.ivos.geofence.service;

import com.tedu.ivos.geofence.pojo.dto.GeofenceParam;
import com.tedu.ivos.geofence.pojo.dto.GeofenceQuery;
import com.tedu.ivos.geofence.pojo.vo.GeofenceVO;

import java.util.List;

public interface GeofenceService {
    List<GeofenceVO> selectGeofence(GeofenceQuery geofenceQuery);

    void saveGeofence(GeofenceParam geofenceParam);

    void deleteGeofence(Long id);
}
