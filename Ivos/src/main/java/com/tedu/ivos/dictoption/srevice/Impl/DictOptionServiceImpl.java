package com.tedu.ivos.dictoption.srevice.Impl;


import com.tedu.ivos.base.exception.ServiceException;
import com.tedu.ivos.base.response.StatusCode;
import com.tedu.ivos.dict.mapper.DictMapper;
import com.tedu.ivos.dict.pojo.dto.DictQuery;
import com.tedu.ivos.dict.pojo.vo.DictVO;
import com.tedu.ivos.dictoption.mapper.DictOptionMapper;
import com.tedu.ivos.dictoption.pojo.dto.DictOptionQuery;
import com.tedu.ivos.dictoption.pojo.dto.DictOptionSaveParam;
import com.tedu.ivos.dictoption.pojo.entity.DictOption;
import com.tedu.ivos.dictoption.pojo.vo.DictOptionVO;
import com.tedu.ivos.dictoption.srevice.DictOptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class DictOptionServiceImpl implements DictOptionService {

    @Autowired
    DictOptionMapper dictOptionMapper;

    @Autowired
    DictMapper dictMapper;
    @Override
    public List<DictOptionVO> selectDictOption(DictOptionQuery dictOptionQuery) {
        log.debug("查询字典项信息业务，参数：{}",dictOptionQuery);
        List<DictOptionVO> list = dictOptionMapper.selectDictOption(dictOptionQuery);
        return list;
    }

    @Override
    public void saveDictOption(DictOptionSaveParam dictOptionSaveParam) {
        log.debug("新增字典项业务，dictOptionSaveParam={}",dictOptionSaveParam);
        DictOption dictOption = new DictOption();
        BeanUtils.copyProperties(dictOptionSaveParam,dictOption);
        if(dictOption.getId()==null){//新增
            dictOption.setCreateTime(new Date());
            dictOptionMapper.insert(dictOption);
        }else{//更新
            dictOption.setUpdateTime(new Date());
            dictOptionMapper.update(dictOption);

        }

    }

    @Override
    public void deleteDictOption(Long id) {
        log.debug("删除字典项业务，参数：{}",id);
        dictOptionMapper.deleteById(id);
    }

    @Override
    public List<DictOptionVO> selectDictOptionByCode(String code) {
        log.debug("根据字典code查询该字典下所有字典项，参数：{}",code);
        //需要先判断该字典是否存在，如果存在，在查询该字典下的字典项
        DictQuery dictQuery = new DictQuery();//封装字典查询对象
        dictQuery.setCode(code);//给字典查询对象设置字典编码
        List<DictVO> dictVOList = dictMapper.selectDict(dictQuery);//查询字典
        if(dictVOList != null&&dictVOList.size()>0){//根据code查询字典存在
            DictVO dictVO = dictVOList.get(0);//获取当前code对应的那一个字典
            DictOptionQuery dictOptionQuery = new DictOptionQuery();
            dictOptionQuery.setDictId(dictVO.getId());//给字典项查询对象设置字典id
            List<DictOptionVO> dictOptionVOList = dictOptionMapper.selectDictOption(dictOptionQuery);
            return dictOptionVOList;
        }else{
            throw new ServiceException(StatusCode.DATA_UNEXISTS);
        }

    }
}
