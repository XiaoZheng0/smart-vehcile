package com.tedu.ivos.dictoption.srevice;

import com.tedu.ivos.dictoption.pojo.dto.DictOptionQuery;
import com.tedu.ivos.dictoption.pojo.dto.DictOptionSaveParam;
import com.tedu.ivos.dictoption.pojo.vo.DictOptionVO;

import java.util.List;

public interface DictOptionService {
    List<DictOptionVO> selectDictOption(DictOptionQuery dictOptionQuery);

    void saveDictOption(DictOptionSaveParam dictOptionSaveParam);

    void deleteDictOption(Long id);

    List<DictOptionVO> selectDictOptionByCode(String code);
}
