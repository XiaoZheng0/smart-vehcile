package com.tedu.ivos.dictoption.controller;

import com.tedu.ivos.base.response.JsonResult;
import com.tedu.ivos.dictoption.pojo.dto.DictOptionQuery;
import com.tedu.ivos.dictoption.pojo.dto.DictOptionSaveParam;
import com.tedu.ivos.dictoption.pojo.vo.DictOptionVO;
import com.tedu.ivos.dictoption.srevice.DictOptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/dictOption")
public class DictOptionController {

    @Autowired
    DictOptionService dictOptionService;

    @GetMapping("select")
    public JsonResult selectDictOption(DictOptionQuery dictOptionQuery){
        log.debug("查询字典项信息，参数：{}",dictOptionQuery);
        List<DictOptionVO> list = dictOptionService.selectDictOption(dictOptionQuery);
        return JsonResult.ok(list);
    }

    @PostMapping("save")
    public JsonResult saveDictOption(DictOptionSaveParam dictOptionSaveParam){
        log.debug("新增字典项，dictOptionSaveParam={}",dictOptionSaveParam);
        dictOptionService.saveDictOption(dictOptionSaveParam);
        return JsonResult.ok();
    }

    @PostMapping("delete/{id}")
    public JsonResult deleteDictOption(@PathVariable Long id){
        log.debug("删除字典项信息,参数：{}",id);
        dictOptionService.deleteDictOption(id);
        return JsonResult.ok();
    }

    /* 定义一个根据字典code查询该字典下所有字典项的方法 */
    @GetMapping("select/{code}")
    public JsonResult selectDictOptionByCode(@PathVariable String code){
        log.debug("根据字典code查询该字典下所有字典项，参数：{}",code);
        List<DictOptionVO> list =dictOptionService.selectDictOptionByCode(code);
        return JsonResult.ok(list);
    }
}
