package com.tedu.ivos.dict.controller;

import com.tedu.ivos.base.response.JsonResult;
import com.tedu.ivos.dict.pojo.dto.DictQuery;
import com.tedu.ivos.dict.pojo.dto.DictSaveParam;
import com.tedu.ivos.dict.pojo.vo.DictVO;
import com.tedu.ivos.dict.service.DictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/dict")
public class DictController {

    @Autowired
    DictService dictService;

    @GetMapping("select")
    public JsonResult selectDict(DictQuery dictQuery){
        log.debug("查询字典信息,参数：{}",dictQuery);
        List<DictVO> list = dictService.selectDict(dictQuery);
        return JsonResult.ok(list);
    }

    @PostMapping("save")
    public JsonResult saveDict(DictSaveParam dictSaveParam){
        log.debug("保存字典，参数：{}",dictSaveParam);
        dictService.saveDict(dictSaveParam);
        return JsonResult.ok();
    }

    @PostMapping("delete/{id}")
    public JsonResult deleteDict(@PathVariable Long id){
        log.debug("删除字典信息，参数：{}",id);
        dictService.deleteDict(id);
        return JsonResult.ok();
    }
}
