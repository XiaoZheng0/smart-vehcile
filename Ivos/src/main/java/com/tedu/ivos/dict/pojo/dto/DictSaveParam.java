package com.tedu.ivos.dict.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DictSaveParam {
    @ApiModelProperty(value = "字典id")
    private Long id;
    @ApiModelProperty(value = "字典名称")
    private String name;
    @ApiModelProperty(value = "字典编码")
    private String code;
    @ApiModelProperty(value = "备注信息")
    private String remark;
}
