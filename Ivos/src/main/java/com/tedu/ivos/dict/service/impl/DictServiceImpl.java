package com.tedu.ivos.dict.service.impl;

import com.tedu.ivos.dict.mapper.DictMapper;
import com.tedu.ivos.dict.pojo.dto.DictQuery;
import com.tedu.ivos.dict.pojo.dto.DictSaveParam;
import com.tedu.ivos.dict.pojo.entity.Dict;
import com.tedu.ivos.dict.pojo.vo.DictVO;
import com.tedu.ivos.dict.service.DictService;
import com.tedu.ivos.dictoption.mapper.DictOptionMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class DictServiceImpl implements DictService {

    @Autowired
    DictMapper dictMapper;

    @Autowired
    DictOptionMapper dictOptionMapper;
    @Override
    public List<DictVO> selectDict(DictQuery dictQuery) {
        log.debug("查询字典信息业务，参数：{}",dictQuery);
        List<DictVO> list = dictMapper.selectDict(dictQuery);
        return  list;
    }

    @Override
    public void saveDict(DictSaveParam dictSaveParam) {
        log.debug("保存字典信息业务，参数：{}",dictSaveParam);
        Dict dict = new Dict();
        BeanUtils.copyProperties(dictSaveParam,dict);
        if(dict.getId()==null){//新增
            dict.setCreateTime(new Date());
            dictMapper.insert(dict);
        }else{
            dict.setUpdateTime(new Date());
            dictMapper.update(dict);
        }

    }

    @Override
    public void deleteDict(Long id) {
        log.debug("删除字典信息业务，参数：{}",id);
        dictMapper.deleteById(id);
        //删除字典，也要删除该字典下所有的字典项！
        dictOptionMapper.deleteByDictId(id);
    }
}
