package com.tedu.ivos.dict.service;

import com.tedu.ivos.dict.pojo.dto.DictQuery;
import com.tedu.ivos.dict.pojo.dto.DictSaveParam;
import com.tedu.ivos.dict.pojo.vo.DictVO;

import java.util.List;

public interface DictService {

    List<DictVO> selectDict(DictQuery dictQuery);

    void saveDict(DictSaveParam dictSaveParam);

    void deleteDict(Long id);
}
