package com.tedu.ivos.dict.mapper;

import com.tedu.ivos.dict.pojo.dto.DictQuery;
import com.tedu.ivos.dict.pojo.entity.Dict;
import com.tedu.ivos.dict.pojo.vo.DictVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface DictMapper {

    List<DictVO> selectDict(DictQuery dictQuery);

    void insert(Dict dict);

    void update(Dict dict);

    void deleteById(Long id);
}
